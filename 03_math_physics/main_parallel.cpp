#include <mpi.h>

#include <cmath>

#include "gauss_seidel.hxx"

double f1(double x, double y) { return y * y; }

double f2(double x, double y) { return std::cos(y) + y * (2 - std::cos(1)); }

double f3(double x, double y) { return x * x * x; }

double f4(double x, double y) { return x + 1; }

int main(int argc, char **argv) {
  MPI_Init(&argc, &argv);

  runParallel(f1, f2, f3, f4);

  MPI_Finalize();
  return 0;
}
