#include "gauss_seidel.hxx"
#include <cmath>
#include <cuchar>
#include <functional>
#include <iomanip>
#include <iostream>
#include <iterator>
#include <mpi.h>
#include <vector>

using std::cin;
using std::cout;
using std::fabs;
using std::function;
using std::size_t;

void calculateResult(double *pMatrix, size_t size, double eps,
                     int &iterations) {
  double dm, dmax, temp;
  iterations = 0;

  do {
    dmax = 0;
    for (size_t i = 1; i < size - 1; i++) {
      for (size_t j = 1; j < size - 1; j++) {
        temp = pMatrix[size * i + j];
        pMatrix[size * i + j] =
            0.25 * (pMatrix[size * i + j + 1] + pMatrix[size * i + j - 1] +
                    pMatrix[size * (i + 1) + j] + pMatrix[size * (i - 1) + j]);
        dm = fabs(pMatrix[size * i + j] - temp);

        if (dmax < dm) {
          dmax = dm;
        }
      }
    }
    iterations++;
  } while (dmax > eps);
}

double calculateParallelStep(double *pMatrix, size_t rows, size_t cols,
                             double eps) {
  double dm, dmax, temp;

  dmax = 0;

  for (size_t i = 1; i < rows - 1; i++) {
    for (size_t j = 1; j < cols - 1; j++) {
      size_t baseIndex = cols * i + j;
      temp = pMatrix[baseIndex];
      if (temp > 10) {
        std::cout << "baseIndex = " << baseIndex << "\n";
        std::cout << "pMatrix[ij] = " << pMatrix[baseIndex] << "\n";
      }
      pMatrix[baseIndex] =
          0.25 * (pMatrix[baseIndex + 1] + pMatrix[baseIndex - 1] +
                  pMatrix[cols * (i + 1) + j] + pMatrix[cols * (i - 1) + j]);
      dm = fabs(pMatrix[baseIndex] - temp);

      if (dmax < dm) {
        dmax = dm;
      }
    }
  }

  return dmax;
}

void terminateProcess(double *pMatrix) { delete[] pMatrix; }

void printMatrix(double *pMatrix, size_t rows, size_t cols) {
  cout << std::fixed << std::setprecision(6);
  for (size_t i = 0; i < rows; i++) {
    for (size_t j = 0; j < cols; j++) {
      cout << pMatrix[i * cols + j] << " ";
    }
    cout << "\n";
  }
}

void initializeWithFunctions(double *pMatrix, size_t size,
                             function<double(double, double)> f1,
                             function<double(double, double)> f2,
                             function<double(double, double)> f3,
                             function<double(double, double)> f4) {
  double h = 1.0 / (size - 1);

  for (size_t i = 0; i < size; i++) {
    for (size_t j = 0; j < size; j++) {
      double x = h * i;
      double y = h * j;
      size_t index = i * size + j;

      if (i == 0) {
        pMatrix[index] = f2(x, 1 - y);
      } else if (i == size - 1) {
        pMatrix[index] = f1(x, 1 - y);
      } else if (j == 0) {
        pMatrix[index] = f3(x, y);
      } else if (j == size - 1) {
        pMatrix[index] = f4(x, y);
      } else {
        pMatrix[index] = 0.0;
      }
    }
  }
}

double *initializeSlice(size_t size, size_t start, size_t rowsPerProcess,
                        bool padTop, bool padBottom,
                        function<double(double, double)> f1,
                        function<double(double, double)> f2,
                        function<double(double, double)> f3,
                        function<double(double, double)> f4) {
  int totalPadding = 0;
  if (padTop) {
    totalPadding++;
  }
  if (padBottom) {
    totalPadding++;
  }

  double *pMatrix = new double[(rowsPerProcess + totalPadding) * size];
  double h = 1.0 / (size - 1);

  size_t k = start;

  if (padTop) {
    k++;
  }

  for (size_t i = start; i < start + rowsPerProcess; i++, k++) {
    for (size_t j = 0; j < size; j++) {
      double x = h * i;
      double y = h * j;
      size_t index = (k - start) * size + j;

      if (i == 0) {
        pMatrix[index] = f2(x, 1 - y);
      } else if (i == size - 1) {
        pMatrix[index] = f1(x, 1 - y);
      } else if (j == 0) {
        pMatrix[index] = f3(x, y);
      } else if (j == size - 1) {
        pMatrix[index] = f4(x, y);
      } else {
        pMatrix[index] = 0.0;
      }
    }
  }

  return pMatrix;
}

double *initializeProcess(size_t &size, double &eps,
                          function<double(double, double)> f1,
                          function<double(double, double)> f2,
                          function<double(double, double)> f3,
                          function<double(double, double)> f4) {
  double *pMatrix;
  do {
    // cout << "Enter the grid size: ";
    cin >> size;
    // cout << "Grid size is " << size << "\n";

    if (size <= 2) {
      cout << "Grid size must me >= 2!\n";
    }
  } while (size <= 2);

  do {
    // cout << "Accuracy: ";
    cin >> eps;
    // cout << "Accuracy is " << eps << "\n";

    if (eps <= 0) {
      cout << "Accuracy must be greater than 0!\n";
    }
  } while (eps <= 0);

  pMatrix = new double[size * size];
  initializeWithFunctions(pMatrix, size, f1, f2, f3, f4);

  return pMatrix;
}

void runSequential(function<double(double, double)> f1,
                   function<double(double, double)> f2,
                   function<double(double, double)> f3,
                   function<double(double, double)> f4) {
  double *pMatrix;
  size_t size;
  double eps;
  int iterations;

  pMatrix = initializeProcess(size, eps, f1, f2, f3, f4);
  // printMatrix(pMatrix, size, size);

  // cout << "Result:\n";

  calculateResult(pMatrix, size, eps, iterations);
  // printMatrix(pMatrix, size, size);

  terminateProcess(pMatrix);
}

void runParallel(function<double(double, double)> f1,
                 function<double(double, double)> f2,
                 function<double(double, double)> f3,
                 function<double(double, double)> f4) {
  // Get rank and world size
  int rank, world;

  MPI_Comm_size(MPI_COMM_WORLD, &world);
  MPI_Comm_rank(MPI_COMM_WORLD, &rank);

  double start = MPI_Wtime();

  // Initialize communicator
  MPI_Comm comm;
  int dims[1] = {world};
  int periods[1] = {false};
  MPI_Cart_create(MPI_COMM_WORLD, 1, dims, periods, 0, &comm);

  // Get neighbours
  int up, down;
  MPI_Cart_shift(comm, 0, 1, &up, &down);

  // Read input params
  int size;
  double eps;

  if (rank == 0) {
    std::cin >> size;
    std::cin >> eps;
  }
  MPI_Bcast(&size, 1, MPI_INT, 0, comm);
  MPI_Bcast(&eps, 1, MPI_DOUBLE, 0, comm);

  int rowsPerProcess = size / world;
  bool padTop = (up == MPI_PROC_NULL) ? false : true;
  bool padBottom = (down == MPI_PROC_NULL) ? false : true;

  // Initialize data slice
  auto slice = initializeSlice(size, rank * rowsPerProcess, rowsPerProcess,
                               padTop, padBottom, f1, f2, f3, f4);

  // Run main loop:
  // 1. Even processes send and receive data, odd - receive and send
  // 2. Run step of a gauss-seidel method on each process
  // 3. Get dmax from each process
  // 4. Stop if maximum of dmax is less than eps
  MPI_Status status;

  auto topShift = (up == MPI_PROC_NULL) ? 0 : size;

  double dmax;
  double maxDmax;
  int iterations = 0;

  do {
    if (rank % 2 == 0) {
      // Send, then receive
      // Send first *actual* row to upper neighbour
      MPI_Send(slice + topShift, size, MPI_DOUBLE, up, 0, comm);
      // Send the last *actual* row to the downer neighbour
      MPI_Send(slice + (size * (rowsPerProcess - 1)), size, MPI_DOUBLE, down, 0,
               comm);

      MPI_Recv(slice, size, MPI_DOUBLE, up, 0, comm, &status);
      MPI_Recv(slice + topShift + (size * rowsPerProcess), size, MPI_DOUBLE,
               down, 0, comm, &status);
    } else {
      // Receive, then send
      MPI_Recv(slice, size, MPI_DOUBLE, up, 0, comm, &status);
      MPI_Recv(slice + topShift + (size * rowsPerProcess), size, MPI_DOUBLE,
               down, 0, comm, &status);

      // Send the last *actual* row to the downer neighbour
      MPI_Send(slice + size * (rowsPerProcess), size, MPI_DOUBLE, down, 0,
               comm);
      // Send first *actual* row to upper neighbour
      MPI_Send(slice + topShift, size, MPI_DOUBLE, up, 0, comm);
    }

    size_t topPadding = (up == MPI_PROC_NULL) ? 0 : 1;
    size_t bottomPadding = (down == MPI_PROC_NULL) ? 0 : 1;

    dmax = calculateParallelStep(
        slice, rowsPerProcess + topPadding + bottomPadding, size, eps);

    // Get the maximum of all dmaxes
    MPI_Reduce(&dmax, &maxDmax, 1, MPI_DOUBLE, MPI_MAX, 0, comm);
    MPI_Bcast(&maxDmax, 1, MPI_DOUBLE, 0, comm);
    iterations++;
  } while (maxDmax > eps);

  // Merge slices together
  if (rank == 0) {
    // std::cout << "Max dmax is " << maxDmax << "\n";
    // std::cout << "Iterations: " << iterations << "\n";

    double *resultMatrix = new double[size * size];
    std::copy(slice, slice + rowsPerProcess * size, resultMatrix);

    for (int i = 1; i < world; i++) {
      MPI_Recv(resultMatrix + rowsPerProcess * i * size, rowsPerProcess * size,
               MPI_DOUBLE, i, 0, MPI_COMM_WORLD, &status);
    }

    // printMatrix(resultMatrix, size, size);
    delete[] resultMatrix;
  } else {
    MPI_Send(slice + topShift, rowsPerProcess * size, MPI_DOUBLE, 0, 0,
             MPI_COMM_WORLD);
  }

  double end = MPI_Wtime();
  double delta = end - start, maxDelta;

  MPI_Reduce(&delta, &maxDelta, 1, MPI_DOUBLE, MPI_MAX, 0, MPI_COMM_WORLD);
  MPI_Barrier(comm);

  if (rank == 0) {
    std::cout << maxDelta << "\n";
  }

  delete[] slice;
}
