#ifndef GAUSS_SEIDEL_SEQ_HXX
#define GAUSS_SEIDEL_SEQ_HXX

#include <cuchar>
#include <functional>

void calculateResult(double *pMatrix, size_t size, double eps, int &iterations);

void terminateProcess(double *pMatrix);

void printMatrix(double *pMatrix, size_t rows, size_t cols);

void initializeDummy(double *pMatrix, size_t size);

double *initializeProcess(size_t &size, double &eps,
                          std::function<double(double, double)>,
                          std::function<double(double, double)>,
                          std::function<double(double, double)>,
                          std::function<double(double, double)>);

void runSequential(std::function<double(double, double)>,
                   std::function<double(double, double)>,
                   std::function<double(double, double)>,
                   std::function<double(double, double)>);

void runParallel(std::function<double(double, double)> f1,
                 std::function<double(double, double)> f2,
                 std::function<double(double, double)> f3,
                 std::function<double(double, double)> f4);

#endif
