#ifndef FILTER_HPP
#define FILTER_HPP

#include <vector>

#include "opencv2/core.hpp"

cv::Mat apply_convolution(cv::Mat &img, cv::Mat &conv_matrix,
                          size_t padding = 0);
cv::Mat merge_images(std::vector<cv::Mat> &slices, size_t padding);
cv::Mat pad_image(cv::Mat &img, size_t padding);
std::vector<cv::Mat> slice_img(cv::Mat &src, size_t n, size_t padding,
                               unsigned short fill_value = 0);

#endif
