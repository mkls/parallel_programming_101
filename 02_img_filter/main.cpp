#include <mpi.h>

#include <iostream>
#include <string>

#include "filter.hpp"
#include "opencv2/imgcodecs.hpp"
#include "opencv2/imgproc.hpp"

using cv::Mat;
using std::string;

int main(int argc, char **argv) {
  if (argc != 4) {
    std::cout << "Usage: apply_filter path/to/image processes kernel_size\n";
    exit(1);
  }

  const int root = 0;

  MPI_Init(&argc, &argv);
  int world_size, rank;
  MPI_Comm_size(MPI_COMM_WORLD, &world_size);
  MPI_Comm_rank(MPI_COMM_WORLD, &rank);

  MPI_Datatype img_bits;

  auto img_path = string(argv[1]);
  auto split_count = std::stoi(string(argv[2]));
  auto kernel_size = std::stoi(string(argv[3]));

  Mat kernel(kernel_size, kernel_size, CV_32F);

  kernel.setTo(cv::Scalar::all(0));
  kernel({cv::Range::all(), cv::Range(kernel_size / 2, kernel_size / 2 + 1)})
      .setTo(cv::Scalar::all(1));
  kernel({cv::Range(kernel_size / 2, kernel_size / 2 + 1), cv::Range::all()})
      .setTo(cv::Scalar::all(1));

  // TODO: use Scatter/Gather
  if (rank == root) {
    auto img = cv::imread(img_path, cv::IMREAD_COLOR);

    // TODO: multiprocessed split?
    auto v = slice_img(img, split_count, kernel_size);

    // MPI_Datatype img_bits;
    MPI_Type_contiguous(v[0].rows * v[0].cols * v[0].channels(), MPI_BYTE,
                        &img_bits);
    MPI_Type_commit(&img_bits);

    // Send image dimensions to other processes
    int buffer_size[2] = {v[0].rows, v[0].cols};
    MPI_Request *reqs = new MPI_Request[split_count - 1];

    for (int i = 1; i < split_count; i++) {
      MPI_Isend(buffer_size, 2, MPI_INT, i, 0, MPI_COMM_WORLD, &reqs[i - 1]);
    }
    MPI_Waitall(split_count - 1, reqs, MPI_STATUSES_IGNORE);

    // Now, when processes now exactly how many bytes they are going to receive,
    // send prepared image slices
    for (int i = 1; i < split_count; i++) {
      MPI_Isend(v[i].data, 1, img_bits, i, 0, MPI_COMM_WORLD, &reqs[i - 1]);
    }
    MPI_Waitall(split_count - 1, reqs, MPI_STATUSES_IGNORE);

    // Apply filter
    auto m = apply_convolution(v[0], kernel, kernel_size);

    // Receive results from other processes
    std::vector<Mat> proccessed_slices(split_count);
    for (size_t i = 1; i < proccessed_slices.size(); i++) {
      proccessed_slices[i] = Mat(m.rows, m.cols, m.type());
    }

    proccessed_slices[0] = m;

    for (int i = 1; i < split_count; i++) {
      MPI_Recv(proccessed_slices[i].data, 1, img_bits, i, 0, MPI_COMM_WORLD,
               MPI_STATUS_IGNORE);
    }

    // Write the result to actual file to check)
    auto result = merge_images(proccessed_slices, kernel_size);
    cv::imwrite("./result.jpg", result);
  } else {
    int sizes[2] = {0, 0};
    MPI_Recv(sizes, 2, MPI_INT, root, 0, MPI_COMM_WORLD, MPI_STATUS_IGNORE);

    MPI_Type_contiguous(sizes[0] * sizes[1] * 3, MPI_BYTE, &img_bits);
    MPI_Type_commit(&img_bits);

    Mat slice(sizes[0], sizes[1], CV_8UC3);
    // auto slice_size = sizes[0] * sizes[1] * 3;

    MPI_Recv(slice.data, 1, img_bits, root, 0, MPI_COMM_WORLD,
             MPI_STATUS_IGNORE);

    auto conv = apply_convolution(slice, kernel, kernel_size);

    MPI_Send(conv.data, 1, img_bits, root, 0, MPI_COMM_WORLD);
  }

  MPI_Finalize();

  return 0;
}
