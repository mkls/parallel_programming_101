#include "../h/sequential.h"

float calc_sum(float start_point, unsigned slices, float step,
               float (*func)(float)) {
    float sum = 0.f;

    for (unsigned i = 0; i < slices; i++) {
        sum += func(start_point + i * step);
    }

    return sum;
}

float trapezoidal(float lower_bound, float upper_bound, unsigned slices,
                  float (*func)(float)) {
    float step = (upper_bound - lower_bound) / slices;
    float sum = (func(lower_bound) + func(upper_bound)) / 2.f;

    // sum += calc_sum(lower_bound + step, upper_bound - step, step, func);
    sum += calc_sum(lower_bound, slices, step, func);

    return step * sum;
}
