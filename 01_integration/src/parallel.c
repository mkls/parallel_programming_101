#include "../h/parallel.h"

#include <mpi.h>
#include <stdio.h>

#include "../h/sequential.h"

void get_bounds(int rank, float slice, float step, float *lower, float *upper) {
    *lower = rank * slice + step;
    *upper = (rank + 1) * slice;
}

float trapezoidal_mpi(float lower_bound, float upper_bound, unsigned slices,
                      int rank, int world_size, float (*func)(float)) {
    float lower, upper;
    float dx = (upper_bound - lower_bound) / slices;
    unsigned slice_per_cpu = (slices - 1) / world_size;

    get_bounds(rank, slice_per_cpu * dx, dx, &lower, &upper);
    slice_per_cpu += (rank == world_size - 1) * (slices - 1) % world_size;

    // float slice_result = calc_sum(lower, upper, dx, func);
    float slice_result = calc_sum(lower, slice_per_cpu, dx, func);
    float total_result;

    MPI_Reduce(&slice_result, &total_result, 1, MPI_FLOAT, MPI_SUM, 0,
               MPI_COMM_WORLD);

    if (rank == 0) {
        total_result += (func(lower_bound) + func(upper_bound)) / 2.f;
        total_result *= dx;

        return total_result;
    }

    return slice_result;
}
