#!/usr/bin/fish

echo 'Executors,N,result,redisual,time,seq' > result.csv

for i in (seq 10000 10000 11000000)
    printf "\r%s" "Testing for N = $i"

    for j in (seq 10)
        printf "%s,%s,%s\n" 6 $i (mpiexec --bind-to core --mca opal_warn_on_missing_libcuda 0 ./bin/integrate -N $i) >> result.csv
    end
    for j in (seq 10)
        printf "%s,%s,%s\n" 1 $i (mpiexec --bind-to core --mca opal_warn_on_missing_libcuda 0 ./bin/integrate -N $i -s) >> result.csv
    end
    for j in (seq 10)
      printf "%s,%s,%s\n" 4 $i (mpiexec -N 4 --mca opal_warn_on_missing_libcuda 0 ./bin/integrate -N $i -s) >> result.csv
    end
end
