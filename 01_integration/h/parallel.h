#ifndef PARALLEL_H
#define PARALLEL_H

float trapezoidal_mpi(float lower_bound, float upper_bound, unsigned slices,
                      int rank, int world_size, float (*func)(float));

#endif
