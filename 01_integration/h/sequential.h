#ifndef SEQUENTIAL_H
#define SEQUENTIAL_H

float trapezoidal(float lower_bound, float upper_bound, unsigned slices,
                  float (*func)(float));

float calc_sum(float start_point, unsigned slices, float step,
               float (*func)(float));

#endif
