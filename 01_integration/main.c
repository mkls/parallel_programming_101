#include <math.h>
#include <mpi.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>

// POSIX only
#include <unistd.h>

#include "./h/parallel.h"
#include "./h/sequential.h"

float func(float x) { return (pow(M_E, x) - 1) / (pow(M_E, x) + 1); }

float residual(float x, float e) { return fabs(x - e) / e; }

int main(int argc, char** argv) {
    int opt;
    int seq = 0;
    unsigned N = 100;
    const float value = 0.867561660966054374;

    while ((opt = getopt(argc, argv, "N:s")) != -1) {
        switch (opt) {
            case 'N':
                N = atoi(optarg);
                break;
            case 's':
                seq = 1;
                break;
            default:
                break;
        }
    }

    MPI_Init(&argc, &argv);

    int world_size, rank;
    MPI_Comm_size(MPI_COMM_WORLD, &world_size);
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);

    if (seq == 1 && rank == 0) {
        double start = clock();
        float res = trapezoidal(0, 2, N, func);
        double end = clock();

        printf("%.8f,%.8f,%.6f,1\n", res, residual(res, value),
               (end - start) / CLOCKS_PER_SEC);
    } else {
        double start = MPI_Wtime();
        float mpi_res = trapezoidal_mpi(0, 2, N, rank, world_size, func);
        double end = MPI_Wtime();

        double delta_time = end - start;
        double max_delta_time;

        MPI_Reduce(&delta_time, &max_delta_time, 1, MPI_DOUBLE, MPI_MAX, 0,
                   MPI_COMM_WORLD);

        if (rank == 0) {
            printf("%.8f,%.8f,%.6f,0\n", mpi_res, residual(mpi_res, value),
                   max_delta_time);
        }
    }

    MPI_Finalize();

    return 0;
}
