#!/usr/bin/fish

set EXEC_PATH "./builddir/apply_filter"
set ITER_PER_RUN 100
set TIME_BIN /usr/bin/time
set TIME_OPTS -f%e
set KERNEL_SIZE 7
set OUT_FILE './result.csv'


# CSV Header
echo 'n,img,time' > result.csv

for i in ./test_imgs/*.jpg
    printf "\r%s" "Testing "$i

    for n in 1 4 6
        printf "\nn = %s\n" $n
        for k in (seq $ITER_PER_RUN)
            printf "iter: "$k"\r"
            printf "%s,%s," $n $i >> $OUT_FILE
            $TIME_BIN $TIME_OPTS $EXEC_PATH $i $n $KERNEL_SIZE &>> $OUT_FILE
            # printf "\n" >> $OUT_FILE
        end
    end

    echo "finished benchmarking "$i
end
