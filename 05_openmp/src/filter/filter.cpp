#include "filter.hpp"

#include "opencv2/core.hpp"
#include "opencv2/imgcodecs.hpp"
#include "opencv2/imgproc.hpp"

using cv::Mat;
using cv::Rect;
using cv::Vec3b;
using std::vector;

Mat apply_convolution(Mat &img, Mat &conv_matrix, size_t padding) {
  Mat result(img.size(), img.type());
  float blue = 0;
  float green = 0;
  float red = 0;

  for (size_t i = padding; i < result.rows - padding; i++) {
    for (size_t j = padding; j < result.cols - padding; j++) {
      blue = 0.;
      green = 0.;
      red = 0.;

      // Reset pixel values
      for (size_t f1 = 0, k = i - padding; f1 < padding && k < i + padding;
           f1++, k++) {
        for (size_t f2 = 0, g = j - padding; f2 < padding && g < j + padding;
             f2++, g++) {
          Vec3b current = img.at<Vec3b>(k, g) * conv_matrix.at<float>(f1, f2);

          if (current[0] >= blue && current[1] >= green && current[2] >= red) {
            blue = current[0];
            green = current[1];
            red = current[2];
          }
        }
      }
      result.at<Vec3b>(i, j) = Vec3b(blue, green, red);
    }
  }

  return result;
}

Mat merge_images(vector<Mat> &slices, size_t padding = 0) {
  Mat result;

  size_t width = 0;
  size_t step = slices[0].size().width - 2 * padding;
  size_t height = slices[0].size().height - 2 * padding;
  auto t = slices[0].type();

  for (auto &img : slices) {
    width += img.size().width - 2 * padding;
  }

  result.create(height, width, t);

  for (size_t i = 0; i < slices.size(); i++) {
    slices[i](Rect(padding, padding, step, height))
        .copyTo(result(Rect(step * i, 0, step, height)));
  }

  return result;
}

Mat pad_image(Mat &img, size_t padding) {
  Mat padded(img.size().width + 2 * padding, img.size().height + 2 * padding,
             img.type());
  cv::copyMakeBorder(img, padded, padding, padding, padding, padding,
                     cv::BORDER_CONSTANT, cv::Scalar(0));

  return padded;
}

vector<Mat> slice_img(Mat &src, size_t n, size_t padding,
                      unsigned short fill_value) {
  vector<Mat> slices(n);

  size_t width = src.size().width / n;
  size_t height = src.size().height;

  for (size_t i = 0; i < n; i++) {
    slices[i].create(height + 2 * padding, width + 2 * padding, src.type());

    // Copy actual image slice
    src(Rect(width * i, 0, width, height))
        .copyTo(slices[i](Rect(padding, padding, width, height)));

    // Apply paddings
    // Up
    slices[i](Rect(0, 0, width + 2 * padding, padding))
        .setTo(cv::Scalar::all(fill_value));

    // Down
    slices[i](Rect(0, height + padding, width + 2 * padding, padding))
        .setTo(cv::Scalar::all(fill_value));

    // Left
    if (i == 0) {
      slices[i](Rect(0, padding, padding, height))
          .setTo(cv::Scalar::all(fill_value));
    } else {
      src(Rect(width * i - padding, 0, padding, height))
          .copyTo(slices[i](Rect(0, padding, padding, height)));
    }

    // Right
    if (i < n - 1) {
      src(Rect((i + 1) * width, 0, padding, height))
          .copyTo(slices[i](Rect(width + padding, padding, padding, height)));
    } else {
      slices[i](Rect(width + padding, padding, padding, height))
          .setTo(cv::Scalar::all(fill_value));
    }
  }

  return slices;
}
