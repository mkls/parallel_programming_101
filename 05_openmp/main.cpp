#include <iostream>
#include <omp.h>
#include <string>

#include "filter.hpp"
#include "opencv2/imgcodecs.hpp"
#include "opencv2/imgproc.hpp"

using cv::Mat;
using std::string;
using std::vector;

int main(int argc, char **argv) {
  if (argc != 4) {
    std::cout << "Usage: apply_filter path/to/image processes kernel_size\n";
    exit(1);
  }

  auto img_path = string(argv[1]);
  auto split_count = std::stoi(string(argv[2]));
  auto kernel_size = std::stoi(string(argv[3]));

  vector<Mat> v;
  vector<Mat> proccessed_slices(split_count);
  Mat kernel(kernel_size, kernel_size, CV_32F);

  omp_set_num_threads(split_count);

  kernel.setTo(cv::Scalar::all(0));
  kernel({cv::Range::all(), cv::Range(kernel_size / 2, kernel_size / 2 + 1)})
      .setTo(cv::Scalar::all(1));
  kernel({cv::Range(kernel_size / 2, kernel_size / 2 + 1), cv::Range::all()})
      .setTo(cv::Scalar::all(1));

  auto img = cv::imread(img_path, cv::IMREAD_COLOR);

  v = slice_img(img, split_count, kernel_size);
  // Apply filter
#pragma omp parallel for if (split_count > 1) schedule(dynamic)                \
    shared(v, proccessed_slices, kernel, kernel_size)
  for (size_t i = 0; i < size_t(split_count); ++i) {
    proccessed_slices[i] = apply_convolution(v[i], kernel, kernel_size);
  }

  // Write the result to actual file to check)
  std::cout << "Writing to file\n";
  auto result = merge_images(proccessed_slices, kernel_size);
  cv::imwrite("./result.jpg", result);

  return 0;
}
